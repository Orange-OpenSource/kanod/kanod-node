#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from cloudinit import subp
from kanod_configure import common


def get_k8s_pause_container(k8s_version):
    pause_container = ""

    ret = subp.subp(['kubeadm', 'config', 'images', 'list',
                     '--kubernetes-version', k8s_version])
    containers = ret[0]
    for container in containers.split('\n'):
        if "k8s.gcr.io/pause:" in container or \
           "registry.k8s.io/pause:" in container:
            pause_container = container
    return pause_container


def local_pause_container(arg: common.RunnableParams):
    '''Gives back the full name to the pause container

    This is the one used by current kubernetes version. It is used
    to be injected in CRI configuration.'''
    nexus = arg.conf.get('nexus', {})
    registry = nexus.get('docker', None)
    k8s_version = arg.system.get('k8s_version', 'v1.18.8')
    pause_container = get_k8s_pause_container(k8s_version)
    print(f'configure pause container with : {pause_container}')
    if registry is None:
        return pause_container
    else:
        [_, path] = pause_container.split('/', 1)
        return f'{registry}/{path}'
