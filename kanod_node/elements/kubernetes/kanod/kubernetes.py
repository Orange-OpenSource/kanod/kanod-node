#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from os import path
import re
import requests
import shutil
import subprocess
import sys
import tempfile
import yaml

from cloudinit import subp
from kanod_configure import common

ADMIN_KUBECONFIG = '/etc/kubernetes/admin.conf'


def k8s_postconfig(arg: common.RunnableParams):
    conf = arg.conf
    '''Configure kubernetes (post kubeadm)'''
    export_kubeconfig(conf)
    if conf.get('kubernetes', {}).get('untaint', False):
        print('remove taint on control plane nodes')
        untaint(arg)


common.register('Kubernetes post config', 300, k8s_postconfig)


def configure_bridge(conf):
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role != 'standalone':
        print('* configured only on standalone')
        return
    standalone = k8s_vars.get('standalone', {})
    pod_subnet = standalone.get('pod_subnet', '192.168.192.0/18')
    common.render_template(
        'cni-bridge.tmpl',
        '/etc/cni/net.d/cni.conflist',
        {'pod_subnet': pod_subnet})


def configure_calico(arg):
    conf = arg.conf
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role != 'standalone':
        print('* configured only on standalone')
        return
    standalone = k8s_vars.get('standalone', {})
    version = standalone.get('calico', None)
    if version is None:
        configure_bridge(conf)
        return
    nexus = conf.get('nexus', {})
    maven = nexus.get('maven', None)
    pod_subnet = standalone.get('pod_subnet', '192.168.192.0/18')
    ip_autodetection_method = k8s_vars.get('ip_autodetection_method', None)
    if maven is None:
        print('* need maven for calico')
        return
    certificate = nexus.get('certificate', None)
    if certificate is None:
        verify = True
    else:
        with tempfile.NamedTemporaryFile('w', delete=False) as fd:
            verify = fd.name
            fd.write(certificate)
            fd.write('\n')
    with tempfile.TemporaryDirectory() as temp_dir:
        url = (
            '{0}/repository/kanod/kanod/calico/{1}/'
            'calico-{1}.yaml').format(maven, version)
        req = requests.get(url, allow_redirects=True, verify=verify)
        with open(path.join(temp_dir, 'calico.yaml'), 'wb') as file:
            file.write(req.content)
        if isinstance(verify, str):
            os.remove(verify)
        common.render_template(
            'fix_pod_cidr.tmpl',
            path.join(temp_dir, 'fix_pod_cidr.yaml'),
            {
                'pod_subnet': pod_subnet,
                'ip_autodetection_method': ip_autodetection_method
            })
        common.render_template(
            'kustomization_calico.tmpl',
            path.join(temp_dir, 'kustomization.yaml'),
            {'ip_autodetection_method': ip_autodetection_method})
        command = [
            'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'apply',
            '-k', temp_dir
        ]
        subp.subp(command)


common.register('Calico standalone configuration', 350, configure_calico)


def export_kubeconfig(conf):
    print('export kubeconfig')
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role is None:
        return
    if role in ['control', 'standalone']:
        admin_username = conf.get('admin', {}).get('username', 'admin')
        kubeconfig_path = f'home/{admin_username}/kubeconfig'
        target = path.join(common.ROOT, kubeconfig_path)
        shutil.copyfile(ADMIN_KUBECONFIG, target)
        shutil.chown(target, user=admin_username)


def launch_kubeadm(system, name, k8s_vars, nexus_registry, container_engine):
    print('launch kubeadm')
    criSocket = None
    target_ip = k8s_vars.get('endpoint')
    standalone = k8s_vars.get('standalone', {})
    pod_subnet = standalone.get('pod_subnet', '192.168.192.0/18')
    service_subnet = standalone.get('service_subnet', '10.96.0.0/12')
    k8s_version = system.get('k8s_version', 'v1.18.8')
    cluster_name = k8s_vars.get('cluster_name', 'cluster')
    dns_domain = standalone.get('dns_domain', f'{cluster_name}.local')
    api_server_extra_args = standalone.get('api_server_extra_args', None)
    kc = standalone.get('kubelet_config', None)
    kubelet_config = yaml.dump(kc) if kc is not None else {}

    kubelet_config["cgroupDriver"] = "systemd"
    nr = standalone.get('node_registration', None)
    node_registration = yaml.dump(nr) if nr is not None else None

    if container_engine == "containerd":
        criSocket = "unix:///run/containerd/containerd.sock"
    if container_engine == "crio":
        criSocket = "unix:///var/run/crio/crio.sock"
    kubelet_config["containerRuntimeEndpoint"] = criSocket
    kubelet_config["imageServiceEndpoint"] = criSocket
    config_param = {'target_ip': target_ip, 'pod_subnet': pod_subnet,
                    'service_subnet': service_subnet, 'dns_domain': dns_domain,
                    'k8s_version': k8s_version,
                    'nexus_registry': nexus_registry,
                    'cluster_name': cluster_name,
                    'criSocket': criSocket, 'name': name,
                    'api_server_extra_args': api_server_extra_args,
                    'kubelet_config': kubelet_config,
                    'node_registration': node_registration}

    with tempfile.NamedTemporaryFile(
        mode='w', delete=False, encoding='utf-8'
    ) as config_tmpfile:
        common.render_template(
            'kubeadm_config.tmpl',
            config_tmpfile.name,
            config_param)

        command = ['kubeadm', 'init', '--config', config_tmpfile.name]

        print("command : " + " ".join(command))
        print(f'  with config params : {config_param}')

        proc = subprocess.run(
            command, stdout=sys.stdout, stderr=subprocess.STDOUT,
            env={"PATH": os.environ.get("PATH", "")})
        if proc.returncode != 0:
            print('kubernetes launch failed')
            raise Exception('kubeadm failed')


def untaint(arg: common.RunnableParams):
    '''Taint the node so that pods can be deployed on masters'''

    k8s_version = arg.system.get('k8s_version', 'v1.18.8')
    k8s_version_minor = int(k8s_version.split('.')[1])
    command = ['kubectl', '--kubeconfig', ADMIN_KUBECONFIG,
               'taint', 'nodes', '--all']

    if k8s_version_minor < 24:
        command.append('node-role.kubernetes.io/master-')
    elif k8s_version_minor == 24:
        command.append('node-role.kubernetes.io/master-')
        command.append('node-role.kubernetes.io/control-plane-')
    elif k8s_version_minor > 24:
        command.append('node-role.kubernetes.io/control-plane-')

    print("command : " + " ".join(command))
    subp.subp(command)


def launch_keepalived(arg: common.RunnableParams):
    k8s_vars = arg.conf.get('kubernetes', {})
    endpoint = k8s_vars.get('endpoint')
    keepalived = k8s_vars.get('keepalived', None)
    if keepalived is None:
        print('* not configured')
        return
    k8s_itf = keepalived.get('interface', None)
    router_id = keepalived.get('router_id', None)
    if router_id is None:
        address_re = re.compile(r'[0-9]*\.[0-9]*\.[0-9]*\.([0-9]*)')
        match = address_re.match(endpoint)
        if match is not None:
            router_id = match.group(1)
        else:
            router_id = '2'
    common.render_template(
        'keepalived.tmpl', 'etc/keepalived/keepalived.conf',
        {'k8s_itf': k8s_itf, 'endpoint': endpoint, 'router_id': router_id})
    try:
        requests.get(
            'https://{}:6443/healthz'.format(endpoint),
            timeout=10, verify=False)
    except:    # noqa: disable=E722
        command = ['systemctl', 'start', 'keepalived']
        subp.subp(command)
    command = ['systemctl', 'enable', '--now', 'monitor-keepalived.service']
    subp.subp(command)


common.register('keepalived', 180, launch_keepalived)


def vault_register(arg: common.RunnableParams):
    '''Register Kubernetes in vault

    This token is consumed by the updaters to create the approle tokens
    for launched clusters and by argocd for manifest specialization.
    '''

    vault_save = arg.system.get('vault_save', None)
    config = arg.conf
    if vault_save is None:
        print('* No vault found')
        return
    if vault_save.get('vault_token', None) is None:
        print('* No token found')
        return
    k8s_cfg = config.get('kubernetes', {})
    vault_name = k8s_cfg.get('vault_name', None)
    if vault_name is None:
        print('* Registration of k8s in vault not configured')
        return
    address = k8s_cfg.get('endpoint', '')
    environ = {
        'KUBECONFIG': '/etc/kubernetes/admin.conf',
        'VAULT_URL': vault_save['vault_url'],
        'LCM_TOKEN': vault_save['vault_token'],
        'VAULT_CA': vault_save['vault_verify'],
        'ADDRESS': address,
        'PORT': '6443',
        'VAULT_NAME': vault_name
    }
    process = subprocess.run(
        ['/usr/local/bin/bind-vault'], env=environ,
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    # print(process.stdout.decode('utf-8'))
    if process.returncode == 0:
        print('* Kubernetes cluster bound to Vault')
    else:
        print('* Vault binding failed')


common.register('Vault registration', 400, vault_register)


def k8s_config(arg: common.RunnableParams):
    '''Configure kubernetes (pre kubadm)'''
    conf = arg.conf
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role is None:
        return
    container_engine = k8s_vars.setdefault('container_engine', 'crio')
    engines = arg.system.get('container-engines', {})
    register_engine = engines.get(container_engine, None)
    if register_engine is None:
        raise Exception(f'unsupported container runtine {container_engine}')
    else:
        register_engine(arg)
    subp.subp(['systemctl', 'enable', 'kubelet'])
    if role != 'standalone':
        common.stop_cloud_init()


common.register('Kubernetes configuration', 200, k8s_config)


def standalone(arg: common.RunnableParams):
    '''standalone post config'''
    conf = arg.conf
    k8s_vars = conf.get('kubernetes', {})
    name = conf.get('name', '')
    role = k8s_vars.get('role', None)
    if role != 'standalone':
        return
    k8s_vars['untaint'] = True
    container_engine = k8s_vars.get('container_engine', None)
    if container_engine is None:
        print('* container engine not defined')
        return
    nexus = conf.get('nexus', None)
    preload_images = k8s_vars.get('preload_images', False)
    if not preload_images and nexus is not None:
        nexus_registry = nexus.get('docker', None)
    else:
        nexus_registry = None
    launch_kubeadm(
        arg.system, name, k8s_vars, nexus_registry, container_engine)


common.register('Standalone configuration', 250, standalone)


def augment_network_ironic(arg: common.RunnableParams):
    print('Specific addons to network for ironic')
    conf = arg.conf
    state = conf.get('network')
    k8s_vars = conf.get('kubernetes', {})
    pxe_itf = k8s_vars.get('pxe_itf', None)
    if pxe_itf is not None and k8s_vars.get('role', '') == 'control':
        bridges = state.setdefault('bridges', [])
        bridges['ironicendpoint'] = {'interfaces': [pxe_itf], 'dhcp4': True}


common.register('* Hook for k8s network tricks', 30, augment_network_ironic)


def report_config(arg: common.RunnableParams):
    conf = arg.conf
    do_report = conf.get('kubernetes', {}).get('report', False)
    if not do_report:
        print('* no reporting')
        return

    nexus = conf.get('nexus', {})
    registry = nexus.get('docker', None)
    repository = nexus.get('maven', None)
    repo_ca = nexus.get('certificate', None)
    vault = conf.get('vault', {})
    vault_url = vault.get('url', None)
    vault_ca = vault.get('ca', None)
    vault_role = vault.get('role', None)
    command = [
        'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'create', 'namespace',
        'kanod']
    subp.subp(command)
    tmpfiles = []
    command = [
        'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'create', 'configmap',
        '--namespace', 'kanod', 'config']

    def add_literal(key, val):
        if val is not None:
            command.append(f'--from-literal={key}={val}')

    def add_file(key, val):
        if val is not None:
            with tempfile.NamedTemporaryFile(
                mode='w+', delete=False, encoding='utf-8'
            ) as fd:
                command.append(f'--from-file={key}={fd.name}')
                tmpfiles.append(fd.name)
                fd.write(val)
                fd.write('\n')
                fd.close()

    proxy = conf.get('proxy', None)
    if proxy is not None:
        add_literal("http_proxy", proxy.get("http", ""))
        add_literal("https_proxy", proxy.get("https", ""))
        add_literal("no_proxy", proxy.get("no_proxy", ""))

    add_literal('registry', registry)
    add_literal('repository', repository)
    add_file('repo_ca', repo_ca)
    add_literal('vault_url', vault_url)
    add_literal('vault_role', vault_role)
    add_file('vault_ca', vault_ca)
    subp.subp(command)
    for file in tmpfiles:
        os.remove(file)


common.register('* Report configuration', 300, report_config)


def launch_manifests(arg: common.RunnableParams):
    conf = arg.conf
    manifests = conf.get('kubernetes', {}).get('manifests', [])
    for manifest in manifests:
        content = manifest.get('content', None)
        if content is not None:
            command = [
                'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'apply',
                '-f', '-']
            proc = subprocess.Popen(
                command, stdin=subprocess.PIPE, encoding='utf-8')
            if proc.stdin is None:
                print('* problem getting stdin')
                continue
            proc.stdin.write(content)
            proc.stdin.close()
            code = proc.wait()
            if code != 0:
                print('* error while applying manifest')
            continue
        url = manifest.get('url', None)
        env2 = manifest.get('variables', {})

        def setenv(var, cfg, field):
            val = cfg.get(field, None)
            if val is not None:
                env2[var] = val

        nexus = conf.get('nexus', {})
        setenv('NEXUS_REGISTRY', nexus, 'docker')
        setenv('NEXUS', nexus, 'maven')
        setenv('REPO_CA', nexus, 'certificate')
        vault = conf.get('vault', {})
        setenv('VAULT_URL', vault, 'url')
        setenv('VAULT_CA', vault, 'ca')
        setenv('VAULT_ROLE', vault, 'role')
        if url is not None:
            command1 = ['curl', '-L', url]
            command2 = ['envsubst']
            command3 = [
                'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'apply',
                '-f', '-']
            p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)
            p2 = subprocess.Popen(
                    command2, stdin=p1.stdout,
                    stdout=subprocess.PIPE, env=env2)
            p3 = subprocess.Popen(
                    command3, stdin=p2.stdout, stdout=subprocess.PIPE)
            code3 = p3.wait()
            if code3 != 0:
                print('* error while applying manifest ', code3)
            continue


common.register('* Launch manifests', 500, launch_manifests)
