kubernetes
==========

Configures a kubernetes node with kubeadm, kubelet, kubectl and crictl
`${DIB_KUBERNETES_VERSION}` contains the kubernetes version and
`${DIB_CRICTL_VERSION}` defines the crictl version (same as Kubernetes version
but 0 as patch version).
