#!/bin/bash

set -eu
echo '* Wait for K8S ready'
for _ in $(seq 10); do
  if kubectl wait --for condition=Ready --timeout 600s --all -n kube-system pod; then
    break
  fi
  sleep 10
  echo -n .
done

echo
echo '* Define vault service account for token review'
kubectl apply -f - <<EOF
---
apiVersion: v1
kind: Namespace
metadata:
  name: vault
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: vault-auth
  namespace: vault
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: role-tokenreview-binding
  namespace: vault
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
  - kind: ServiceAccount
    name: vault-auth
    namespace: vault
---
apiVersion: v1
kind: Secret
metadata:
  name: vault-auth-token
  namespace: vault
  annotations:
    kubernetes.io/service-account.name: vault-auth
type: kubernetes.io/service-account-token
EOF

secret=vault-auth-token
token="$(kubectl get -n vault secret "$secret" -o jsonpath='{.data.token}' | base64 -d)"
ca="$(kubectl get -n vault secret "$secret" -o "jsonpath={.data['ca\.crt']}" | base64 -d)"
ca="${ca//$'\n'/\\n}"

cat > /etc/kanod-configure/bind-vault <<EOF
{
  "kubernetes_host": "https://${ADDRESS}:${PORT}", "kubernetes_ca_cert": "${ca}",
  "token_reviewer_jwt": "${token}"
}
EOF

echo '* Bind Vault'
code=$(curl -X POST -w "%{http_code}" -o /dev/null --insecure \
    "${VAULT_URL}/v1/auth/kubernetes/${VAULT_NAME}/config" \
    --header "X-Vault-Token: ${LCM_TOKEN}" \
    --header 'Content-Type: application/json' \
    --data '@/etc/kanod-configure/bind-vault')

if [ "${code}" != "204" ]; then
  echo "* binding failed"
  exit 1
fi

echo "* binding succeeded"
