# Kanod Node

Library for kanod-image-builder defining the building of an OS image for a Kubernetes node.

Documentation : https://orange-opensource.gitlab.io/kanod/reference/machines/lcm/index.html
