export NEXUS_KANOD_USER=admin
export NEXUS_KANOD_PASSWORD=secret
export NEXUS_REGISTRY=192.168.133.100:8443
export REPO_URL=http://192.168.133.100/repository/kanod
export MAVEN_OPTS=''
export MAVEN_CLI_OPTS=''
export IMAGE_SYNC_DIR=/tmp
export ROLE=node
export OS_DEBUG=false
export OS_VERSION=jammy
export K8S_VERSION=v1.29.4
export VERSION=1.13.7
export NO_SYNC_KUBE_ELEMENT=1
./build.sh
